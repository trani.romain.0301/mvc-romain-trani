<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php //$view->dump($view->getFlash());
foreach ($view->getFlash() as $flash) {
    echo '<p class="'.$flash['type'].'">'.$flash['message'].'</p>';
}
?>
<header id="masthead">
    <nav>
        <ul>
            <li><a href="<?= $view->path(''); ?>">Home</a></li>
            <li><a href="<?= $view->path('les-recettes'); ?>">Mes recettes</a></li>
        </ul>
    </nav>
</header>

<div class="container">
    <?= $content; ?>
</div>

<footer id="colophon">
    <div class="wrap">
        <p>MVC 6 - Admin</p>
    </div>
</footer>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>


<?php if (!empty($category)) {
    foreach ($category as $categorie) { ?>
        <h1><?php echo $categorie->getTitle(); ?></h1>
        <p><?php echo $categorie->getDescription(); ?></p>
    <?php }
} else { ?>
<?php } ?>
