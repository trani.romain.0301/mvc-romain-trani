<?php

namespace Core\Helper;

class Debugger
{
    /**
     * @param $var
     * @return void
     */
    public static function debug($var) {
        echo '<pre style="height:100px;overflow-y: scroll;font-size:.8em;padding: 10px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
        print_r($var);
        echo '</pre>';
    }
}



//CRUD complet dans l'admin
//    category :
//        - id
//        - title VARCHAR
//        - description TEXT
//        - status ( draft, publish )
//        - created_at
//        - modified_at
//
//- listing, single, add, edit, delete
namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class CategoryModel extends AbstractModel
{
    protected static $table = 'category';

    protected $id;
    protected $title;
    protected $description;
    protected $status;
    protected $created_at;
    protected $modified_at;
/
* @return mixed
*/
    public function getId()
    {
        return $this->id;
    }

/
* @return mixed
*/
    public function getTitle()
    {
        return $this->title;
    }

/
* @return mixed
*/
    public function getDescription()
    {
        return $this->description;
    }

/
* @return mixed
*/
    public function getStatus()
    {
        return $this->status;
    }


/
* @return mixed
*/
    public function getCreated()
    {
        return $this->created_at;
    }
/
* @return mixed
*/
    public function getModified()
    {
        return $this->modified_at;
    }



